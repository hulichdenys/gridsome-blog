module.exports = {
  siteName: 'Gridsome',
  icon: 'src/assets/favicon.png',
  plugins: [
    /**
     * Gridsome plugin typescript
     * Doc: https://gridsome.org/plugins/gridsome-plugin-typescript
     */
    { use: 'gridsome-plugin-typescript' },
    /**
     * Gridsome resources loader plugin
     * Doc: https://gridsome.org/plugins/gridsome-plugin-sass-resources-loader
     */
    {
      use: 'gridsome-plugin-sass-resources-loader',
      options: {
        resources: [
          '~/assets/scss/abstracts/_mixins.scss',
          '~/assets/scss/abstracts/_variables.scss',
          'bootstrap/scss/_functions.scss',
          'bootstrap/scss/_variables.scss',
          'bootstrap/scss/_mixins.scss',
        ],
      },
    },
    /**
     * Storyblock source for Gridsome
     * Doc: https://gridsome.org/plugins/gridsome-source-storyblok
     */
    {
      use: 'gridsome-source-storyblok',
      options: {
        client: {
          accessToken: 'tcTL29PGGVyZDYQkwYZQxQtt',
        },
      },
    },
    /**
     * Gridsome i18n plugin
     * Doc: https://gridsome.org/plugins/gridsome-plugin-i18n
     */
    {
      use: 'gridsome-plugin-i18n',
      options: {
        locales: ['en', 'ru'], // locales list
        fallbackLocale: 'en', // fallback language
        defaultLocale: 'en', // default language
        enablePathRewrite: true, // rewrite path with locale prefix, default: true
        rewriteDefaultLanguage: false, // rewrite default locale, default: true
        messages: {
          en: require('./src/locales/en.json'), // Messages files
          ru: require('./src/locales/ru.json'),
        },
        enablePathGeneration: false, // disable path generation
        routes: {}, // disable path generation
      },
    },
  ],
  chainWebpack: (config) => {
    const svgRule = config.module.rule('svg');
    svgRule.uses.clear();
    svgRule.use('vue-svg-loader').loader('vue-svg-loader');
  },
};
