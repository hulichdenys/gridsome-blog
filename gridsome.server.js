// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api

// Changes here requires a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const PER_PAGE = 8;

module.exports = function (api) {
  api.createPages(async ({ graphql, createPage }) => {
    const { data } = await graphql(`
      {
        pages: allStoryblokEntry {
          edges {
            node {
              id
              content
              lang
              full_slug
            }
          }
        }

        posts: allStoryblokEntry(
          filter: { full_slug: { regex: "posts|posty" } }
        ) {
          edges {
            node {
              id
              content
              lang
              full_slug
            }
            next {
              id
            }
            previous {
              id
            }
          }
        }
      }
    `);

    data.pages.edges.forEach(({ node }) => {
      if (
        node.content.component === 'Category' ||
        node.content.component === 'Tag' ||
        node.content.component === 'Author'
      ) {
        createPage({
          path: `/${node.full_slug}`,
          component: `./src/templates/Category.vue`,
          context: {
            id: node.id,
            lang: node.lang,
            posts: node.content.posts,
            perPage: PER_PAGE,
          },
          route: {
            meta: {
              locale: node.lang === 'default' ? 'en' : 'ru',
            },
          },
        });
      } else if (node.content.component === 'Home') {
        createPage({
          path: `/${node.lang === 'default' ? '' : `${node.lang}`}`,
          component: `./src/templates/${node.content.component}.vue`,
          context: {
            id: node.id,
            lang: node.lang,
            posts: node.content.posts,
            perPage: PER_PAGE,
          },
          route: {
            meta: {
              locale: node.lang === 'default' ? 'en' : 'ru',
            },
          },
        });
      }
    });

    data.posts.edges.forEach(({ node, next, previous }) => {
      createPage({
        path: `/${node.full_slug}`,
        component: `./src/templates/${node.content.component}.vue`,
        context: {
          id: node.id,
          lang: node.lang,
          categories: node.content.categories,
          tags: node.content.tags,
          author: node.content.author,
          previousId: previous && previous.id,
          nextId: next && next.id,
        },
        route: {
          meta: {
            locale: node.lang === 'default' ? 'en' : 'ru',
          },
        },
      });
    });
  });
};
