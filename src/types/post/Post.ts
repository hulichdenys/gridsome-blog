import { PostContent } from './PostContent';

export interface Post {
  id?: string;
  full_slug?: string;
  first_published_at?: any;
  content: PostContent;
}
