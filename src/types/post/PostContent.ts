import { Meta } from '~/types/meta/Meta';
import { PostImage } from './PostImage';
import { PostRichtextContent } from './PostRichtextContent';

export interface PostContent {
  _uuid: string;
  meta: [Meta];
  tags: [string];
  categories: [string];
  title: string;
  images: [PostImage];
  content: PostRichtextContent;
  short_content: PostRichtextContent;
  component: string;
  _editable: string;
}
