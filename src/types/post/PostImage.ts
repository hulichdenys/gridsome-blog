export interface PostImage {
  id: number;
  alt: string;
  name: string;
  focus: any;
  title: string;
  filename: string;
  copyright: any;
}
