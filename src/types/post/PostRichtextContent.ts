interface InnerRichtextContent {
  content: [any];
  type: string;
}

export interface PostRichtextContent {
  content: [InnerRichtextContent];
  type: string;
}
