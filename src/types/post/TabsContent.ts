import { Category } from '~/types/pages/Category';
import { Author } from '~/types/pages/Author';
import { Post } from './Post';

export interface TabsContent {
  first_published_at?: Date;
  categories: { node: Category }[];
  tags: { node: Category }[];
  author: { node: Author }[];
  previous: Post | null;
  next: Post | null;
}
