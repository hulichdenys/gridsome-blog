export interface MetaTags {
  _uid: string;
  title: string;
  description: string;
  plugin: string;
  og_image: string;
  og_title: string;
  og_description: string;
  twitter_image: string;
  twitter_title: string;
  twitter_description: string;
}
