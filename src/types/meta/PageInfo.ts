export interface PageInfo {
  totalPages: number;
  currentPage: number;
}
