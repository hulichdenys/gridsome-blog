import { MetaTags } from './MetaTags';

export interface Meta {
  _uid: string;
  tags: MetaTags;
  component: string;
  _editable: string;
}
