import { Meta } from '~/types/meta/Meta';

export interface HomeContent {
  _uuid: string;
  meta: [Meta];
  component: string;
  posts: [string];
  _editable: string;
}
