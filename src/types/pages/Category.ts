import { Meta } from '~/types/meta/Meta';

export interface CategoryContent {
  _uuid: string;
  meta: [Meta];
  posts: [string];
  title: string;
  component: string;
  description?: string;
  _editable: string;
}

export interface Category {
  full_slug: string;
  content: CategoryContent;
}
