import { Meta } from '~/types/meta/Meta';
import { PostImage } from '~/types/post/PostImage';

export interface AuthorContent {
  _uid: string;
  meta: [Meta];
  name: string;
  description: string;
  position: string;
  image: PostImage;
  posts: [string];
  component: string;
  _editable: string;
}

export interface Author {
  full_slug: string;
  content: AuthorContent;
}
