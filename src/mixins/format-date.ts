import Vue, { VueConstructor } from 'vue';

import { Post } from '~/types/post/Post';

interface ComponentInstance {
  post: Post;
}

export default (Vue as VueConstructor<Vue & ComponentInstance>).extend({
  computed: {
    /**
     * Format post firs published at date [dd.mm.yy.tt]
     */
    date(): string {
      const date = new Date(this.post.first_published_at);

      return date.toLocaleString();
    },
  },
});
