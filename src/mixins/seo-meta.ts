import Vue, { VueConstructor } from 'vue';

import { MetaTags } from '~/types/meta/MetaTags';

interface ComponentInstance {
  $page: any;
  tags: MetaTags;
}

export default (Vue as VueConstructor<Vue & ComponentInstance>).extend({
  /**
   * Set page meta tags
   */
  metaInfo() {
    return {
      title: this.tags.title,
      meta: [
        // prettier-ignore
        { hid: 'description', name: 'description', content: this.tags.description },
        { hid: 'keywords', name: 'keywords', content: this.tags.description },
        { hid: 'og:title', property: 'og:title', content: this.tags.og_title },
        // prettier-ignore
        { hid: 'og:description', property: 'og:description', content: this.tags.og_description },
        { hid: 'og:image', property: 'og:image', content: this.tags.og_image },
        // prettier-ignore
        { hid: 'og:title', property: 'twitter:title', content: this.tags.twitter_title },
        // prettier-ignore
        { hid: 'og:description', property: 'twitter:description', content: this.tags.twitter_description },
        // prettier-ignore
        { hid: 'og:image', property: 'twitter:image', content: this.tags.twitter_image },
      ],
    };
  },
  computed: {
    /**
     * Get meta tages
     * @returns {MetaTags}
     */
    tags(): MetaTags {
      return this.$page.data.content.meta[0].tags;
    },
  },
});
